# How to contribute

## Contributing code

To contribute code, please create [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and ask
[Martin Vuk](@martin.vuk) to merge it.

The easiest way to create merge request is the following:

* create issue that describes scope of the contribution
* click `Create merge request` button at the bottom of the issue page.

This way the merge request is connected to the issue and issue will be closed as soon as merge request is merged.
